<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">

<!-- Font Awesome Stylesheet -->
<link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">

<!-- Custom Stylesheets -->
<link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
<link rel="stylesheet" id="cpswitch" href="{{asset('frontend/css/orange.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">

<!-- Owl Carousel Stylesheet -->
<link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/owl.theme.css')}}">

<!-- Flex Slider Stylesheet -->
<link rel="stylesheet" href="{{asset('frontend/css/flexslider.css')}}" type="text/css" />

<!--Date-Picker Stylesheet-->
<link rel="stylesheet" href="{{asset('frontend/css/datepicker.css')}}">

<!-- Magnific Gallery -->
<link rel="stylesheet" href="{{asset('frontend/css/magnific-popup.css')}}">

<!-- Color Panel -->
<link rel="stylesheet" href="{{asset('frontend/css/jquery.colorpanel.css')}}">
@extends('frontend.layouts.master')
@section('title','| Search Result')
@section('content')
<div class="container">
    <div style="margin-top: 50px;padding-bottom: 100px">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Photo</a></li>
           <!--  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Map</a></li> -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="clearfix">
                <div class="col-md-4">
                        <img src="{{ asset('storage/hotel/feature_image/' . $hotel->feature_image) }}" alt="" style="height: 500px;margin-top: 20px" width="600px">
                </div>
                <div class="col-md-2">
                                
                </div>
                <div class="col-md-6">
                    @foreach(explode('|', $hotel->images) as $detail )
                    <div class="col-md-12">
                        <a href="{{ asset('storage/hotels/' . $detail) }}" class="widget-products-image">
                            <img src="{{ asset('storage/hotels/' . $detail) }}" style="height: 200px;margin-top: 2px" width="300px">
                            <span class="widget-products-overlay"></span>
                        </a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="bg-transparent">
        <div class="list-group">
            <div class="table-responsive">
                <div class="table-header">
                    <div class="table-caption">
                       <h3>Tourist Areas Hotel Information</h3>
                    </div>
                </div>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Name:</td>
                        <td>{{$hotel->name}}</td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td>{{$hotel->address}}</td>
                    </tr>
                    <tr>
                        <td>Tourist Area:</td>
                        <td>{{$hotel->tourist_area}}</td>
                    </tr>
                    <tr>
                        <td>Car service :</td>
                        <td>{{$hotel->transportation}}</td>
                    </tr>
                    <tr>
                        <td>Area:</td>
                        <td>{{$hotel->area}}</td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>{{$hotel->phone}}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>{{$hotel->email}}</td>
                    </tr>
                    <tr>
                        <td>Website:</td>
                        <td>{{$hotel->website}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        
            <div class="col-sm-3">
            <div class="rating-block">
                <h4>Average user rating</h4>
                <h2 class="bold padding-bottom-7">4.5 <small>/ 5</small></h2>
                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </button>
            </div>
        </div>
        <div class="col-sm-3">
            <h4>User Rating </h4>
            <div class="pull-left">
                <div class="pull-left" style="width:35px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:180px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">1</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:35px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:180px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">1</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:35px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:180px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">0</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:35px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:180px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">0</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:35px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:180px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">0</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">
            <hr/>
            <div class="review-block">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                        <div class="review-block-name">Alex Mile</div>
                        <div class="review-block-date"> 1 day ago</div>
                    </div>
                    <div class="col-sm-9">
                        <div class="review-block-rate">
                            <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default btn-grey btn-xs" aria-label="Left Align">
                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                            </button>
                        </div>
                        <div class="review-block-description" style="margin-top: 10px"> This Hotel environment is good. Stuff are really amazing attitude.  </div>
                    </div>
                </div>
                <hr/>
        </div>
    </div>

</div> <!-- /container -->
    @push('custom_js')
        <style>
            .review-block{
                background-color:#FAFAFA;
                border:1px solid #EFEFEF;
                padding:15px;
                border-radius:3px;
                margin-bottom:15px;
            }
            .rating-block{
                background-color:#FAFAFA;
                border:1px solid #EFEFEF;
                padding:15px 15px 20px 15px;
                border-radius:3px;
            }
            .review-block-name{
                font-size:12px;
                margin:10px 0;
            }
            .review-block-date{
                font-size:12px;
            }
            .review-block-rate{
                font-size:13px;
                margin-bottom:15px;
            }
            .review-block-title{
                font-size:15px;
                font-weight:700;
                margin-bottom:10px;
            }
            .review-block-description{
                font-size:13px;
            }


        </style>
    @endpush
@endsection
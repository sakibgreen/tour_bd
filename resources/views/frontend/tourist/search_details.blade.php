@extends('frontend.layouts.master')
@section('title','| Search Result')
@section('content')
<div class="container">
    <div style="margin-top: 50px;padding-bottom: 100px">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Photo</a></li>
           <!--  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Map</a></li> -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="clearfix">               
                    @foreach(explode('|', $tourist->images) as $detail )
                    <div class="col-md-6">
                        <a href="{{ asset('storage/upload/' . $detail) }}" class="widget-products-image">
                            <img src="{{ asset('storage/upload/' . $detail) }}" style="height: 200px;margin-top: 2px" width="300px">
                            <span class="widget-products-overlay"></span>
                        </a>
                    </div>
                @endforeach
                
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div style="margin-top: 50px;padding-bottom: 100px">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Tourist Area Info</a></li>
            </ul>
 
        </div>
        <div class="bg-transparent">
            <div class="list-group">
                <div class="table-responsive">
                    <div class="table-header">
                        <div class="table-caption">
                            <h3> Tourist Area Info</h3>
                        </div>
                    </div>
                    <table class="table">
                        <tbody>
                        <tr> 
                            <td>Location Name:</td>
                            <td>{{$tourist->location}}</td>
                        </tr>
                        <tr> 
                            <td>Tourist Spot Name:</td>
                            <td>{{$tourist->touristspot}}</td>
                        </tr>
                        <tr> 
                            <td>Details:</td>
                            <td>{{$tourist->details}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>   
        </div>
    </div> 
   
@endsection